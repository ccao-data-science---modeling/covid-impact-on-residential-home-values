# COVID Impact on Residential Home Values

The rapid and dramatic changes brought on by COVID-19 may have substantial impacts on the residential housing market in Cook County. The Assessor is exploring responses to this disruption, including the possibility of adjusting assessed values for the South Triennial area as part of the 2020 re-assessment.

This repository contains the code and data used to produce one method of adjustment.

The logic is that historically, rising unemployment rates are correlated with falling housing prices. Therefore, we could use use projected unemployment rates in each region of Cook County to project that region's overall decline in housing value.

Some data necessary for the replication of these results is available on the Cook County Open Data Portal:

- Cook County Assessor's Residential COVID Adjustments
- Cook County Assessor's Residential Neighborhoods

Here are some highlights of this repository:

- The high-level report is [here](https://gitlab.com/ccao-data-science---modeling/covid-impact-on-residential-home-values/-/blob/master/report/COVID-19-Adjustments%20With%20Centering.html). This report opens in any internet browser. 
- See an excel file of [COVID Labor Market Assumptions.xlsx](https://gitlab.com/ccao-data-science---modeling/covid-impact-on-residential-home-values/-/blob/master/data/raw/small/COVID%20Labor%20Market%20Assumptions.xlsx)
- See the end result: estimated adjustments per township, [summary_by_town.rda](https://gitlab.com/ccao-data-science---modeling/covid-impact-on-residential-home-values/-/blob/master/data/processed/summary_by_town.csv).


More details about the contents of this repository are below.

---

## Producing the final report

The high-level report is produced by [`Policy-Proposal---COVID-19-Adjustments-to-Residential-Property-Values.Rmd`](https://gitlab.com/ccao-data-science---modeling/covid-impact-on-residential-home-values/-/blob/master/Policy%20Proposal%20-%20COVID-19%20Adjustments%20to%20Residential%20Property%20Values.Rmd). You can reproduce it in R using data entirely contained within this repository, because this repository contains both raw and processed data files from other sources and the CCAO. 


### Required R libraries

- `readr`
- `tidyverse`
- `jsonlite`
- `testit`
- `sf`
- `lubridate`
- `readxl`
- `tidyr`
- `htmltools`
- `stargazer`
- `leaflet`
- `ggplot2`
- `gridExtra`
- `scales`
- `kintr`

### To produce the final report in R:

1. Clone the repository.
2. Install packages above if needed.
3. Manually download the CDC's [Social Vulnerability Index](https://svi.cdc.gov/data-and-tools-download.html) into the big raw data folder.
4. Manually download the [Cook County Assessor's Residential COVID Adjustments](https://datacatalog.cookcountyil.gov/Finance-Administration/Cook-County-Assessor-s-Residential-COVID-Adjustmen/sypz-gxn2). Strip out the COVID results, and save in data/raw/big as assessments.rda. 
5. Knit [`Policy-Proposal---COVID-19-Adjustments-to-Residential-Property-Values.Rmd`](https://gitlab.com/ccao-data-science---modeling/covid-impact-on-residential-home-values/-/blob/master/Policy%20Proposal%20-%20COVID-19%20Adjustments%20to%20Residential%20Property%20Values.Rmd).



---

## Directory Structure


- `code.r/` stores all analysis scripts that pull data, conduct filtering and analyses, and produce adjustment values based on COVID Labor Market Assumptions. Note: running the scripts requires a connection to the CCAO Data Science's SQL server.
- `data/` stores data
  - `raw/` stores raw data
    - `small/` stores manually-refreshed data files
    - `big/` stores large data files that are refreshed when `code.r/main.R` script is run
  - `processed/` stores processed data
  - `report/` stores detailed report with township adjustments.
  
  ---



## Acknowledgments

See [Acknowldgements.md](https://gitlab.com/ccao-data-science---modeling/covid-impact-on-residential-home-values/-/blob/master/Acknowledgements.Md).

## Update Notes

Due to the lack of integration across legacy systems and platforms, data sets update throughout the year. At the time this analysis was conducted, the Board of Review had not completed the 2019 Assessment roll. By July, it had, and a new set of data was available in 2020 that contained some changes over the prior year. I updated this code slighltly on a different branch to account for these changes, but I did not merge that branch (8) into production, as it would create system errors. There will be some differences (< 1% of PINs) in the reference values published on the Open Data Portal and their actual 2020 initial values.