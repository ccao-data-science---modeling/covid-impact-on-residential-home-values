
# NEW VALUES ----
new_values <-
  merge(
        subset(assessments
               , select = c("pin10", "CLASS", "TOWN", "NBHD", "TOWN NAME", "census_tract", "triad"
                                       , "triad_name", "group", "REFERENCE", "REFERENCE VALUE"))
        , subset(tract_data
                 , select  = c("TRACTA","COVID-19 Adjustment","Pre-COVID Unemployment Rate"
                               , "Post-COVID Unemployment Rate")),
        by.x="census_tract", by.y="TRACTA", all.x==TRUE)

# We average over CCAO neighborhoods because the institutional environment expects property values to be uniform within these codes
temp <- new_values %>% dplyr::group_by(TOWN, NBHD) %>%
  summarise(`Avg NBHD COVID-19 Adjustment` =  mean(`COVID-19 Adjustment`, na.rm = TRUE)
            , `Avg NBHD Pre-COVID Unemployment` =  mean(`Pre-COVID Unemployment Rate`, na.rm = TRUE)
            , `Avg NBHD Post-COVID Unemployment` =  mean(`Post-COVID Unemployment Rate`, na.rm = TRUE)
            )

new_values <- merge(new_values, temp, by=c("TOWN", "NBHD"))
rm(temp)

new_values$`Avg NBHD COVID-19 Adjustment` <- ifelse(new_values$`Avg NBHD COVID-19 Adjustment`>=-6
                                                    , -6
                                                    ,new_values$`Avg NBHD COVID-19 Adjustment`)

# Adjust Multi-Family relative to Single-Family
if(params$SFvMF==TRUE){
  SFvMFamt_n <- as.numeric(str_remove(params$SFvMFamt, "%"))

  new_values$`Avg NBHD COVID-19 Adjustment` <- ifelse(new_values$group=='2 - 6 Unit Multi Family'
    , new_values$`Avg NBHD COVID-19 Adjustment` + (new_values$`Avg NBHD COVID-19 Adjustment`)*(SFvMFamt_n/100)
    , new_values$`Avg NBHD COVID-19 Adjustment`)

}

new_values$`REFERENCE VALUE` <- as.numeric(new_values$`REFERENCE VALUE`)

new_values$`Adjusted Value` <- new_values$`REFERENCE VALUE`*
  (1+new_values$`Avg NBHD COVID-19 Adjustment`/100)

save(new_values, file = "data/processed/new_values.rda")

# Only publishing south tri FCV
new_values$`Adjusted Value` <- ifelse(new_values$triad!=3, NA, new_values$`Adjusted Value`)
new_values$`COVID-19 Adjustment` <- ifelse(new_values$triad!=3, NA, new_values$`COVID-19 Adjustment`)
# Output to Socrata Open Data
write.csv(new_values[,c(1:12,18)], file = "data/raw/big/assessments.csv", row.names = FALSE, na = '')

